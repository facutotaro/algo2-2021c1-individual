#include "Lista.h"

Lista::Lista()
{
    // Completar
    primero_ = nullptr;
    ultimo_ = nullptr;
    longitud_ = 0;
}




void Lista::_destruir()
{
    while (primero_ != NULL)
    {
        sacarPrimero();
    }
}
void Lista::sacarPrimero()
{
    eliminar(0);
    
}

Lista::~Lista()
{
    // Completar
    _destruir();
}

Lista::Lista(const Lista &l) : Lista()
{
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    this->operator=(l);
}

Lista &Lista::operator=(const Lista &aCopiar)
{
    // Completar
    this->_destruir();
    for (int i = 0; i < aCopiar.longitud(); i++)
    {
        this->agregarAtras(aCopiar.iesimo(i));
    }

    return *this;
}

void Lista::agregarAdelante(const int &elem)
{
    // Completar
    Nodo *p = new Nodo(elem);
    p->siguiente = primero_;
    p->anterior = nullptr;
    p->valor = elem;
    if (longitud_ > 0)
    {
        primero_->anterior = p;
    }
    primero_ = p;
    if (longitud_ == 0)
    {
        ultimo_ = p;
    }
    longitud_++;
}

void Lista::agregarAtras(const int &elem)
{
    // Completar
    Nodo *p = new Nodo(elem);
    p->siguiente = nullptr;
    p->anterior = ultimo_;
    p->valor = elem;
    if (longitud_ > 0)
    {
        ultimo_->siguiente = p;
    }
    ultimo_ = p;
    if (longitud_ == 0)
    {
        primero_ = p;
    }
    longitud_++;
}

void Lista::eliminar(Nat i)
{
    // Completar
    int j = 0;
    Nodo *actual = primero_;
    while (j < i)
    {
        actual = actual->siguiente;
        j++;
    }
    if (longitud_ == 1)
    {
        primero_ = nullptr;
        ultimo_ = nullptr;
    }
    else if (j == longitud_ - 1)
    {
        ultimo_ = actual->anterior;
        (actual->anterior)->siguiente = actual->siguiente;
    }
    else if (j == 0)
    {
        primero_ = actual->siguiente;
        (actual->siguiente)->anterior = actual->anterior;
    }
    else
    {
        (actual->anterior)->siguiente = actual->siguiente;
        (actual->siguiente)->anterior = actual->anterior;
    }
    delete actual;
    longitud_--;
}

int Lista::longitud() const
{
    // Completar
    return longitud_;
}

const int &Lista::iesimo(Nat i) const
{
    // Completar
    int j = 0;
    Nodo *actual = primero_;
    while (j < i)
    {
        actual = actual->siguiente;
        j++;
    }
    return actual->valor;
}

int &Lista::iesimo(Nat i)
{
    // Completar (hint: es igual a la anterior...)
    int j = 0;
    Nodo *actual = primero_;
    while (j < i)
    {
        actual = actual->siguiente;
        j++;
    }
    return actual->valor;
}

void Lista::mostrar(ostream &o)
{
    // Completar
}

Lista::Nodo::Nodo(int elem)
{
    siguiente = nullptr;
    anterior = nullptr;
    valor = elem;
}