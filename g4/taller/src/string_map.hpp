
template <typename T>
string_map<T>::string_map()
{
    raiz = nullptr;
    _size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T> &aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d)
{
    // COMPLETAR
    if(d.size() > 0)
    {
        raiz = new Nodo();
        raiz->copiaNodos(d.raiz);
        _size = d.size();
        return *this;
    }
    else
    {
        raiz = nullptr;
        _size = 0;
        return *this;
    }


}

template <typename T>
void string_map<T>::Nodo::copiaNodos(Nodo *n)
{
    for (int i = 0; i < 256; i++)
    {
        if ((n->siguientes)[i] != nullptr)
        {
            (this->siguientes)[i] = new Nodo();
            if ((n->siguientes)[i]->definicion != nullptr)
            {
                T *copia = new T(*(n->siguientes)[i]->definicion);
                (this->siguientes)[i]->definicion = copia;
            }
            (this->siguientes)[i]->copiaNodos((n->siguientes)[i]);
        }
    }
}

template <typename T>
string_map<T>::~string_map()
{
    eliminar();
}

template <typename T>
void string_map<T>::Nodo::podarNodos()
{
    for (int i = 0; i < 256; i++)
    {
        if (siguientes[i] != nullptr)
        {
            if (siguientes[i]->cantHijos() == 0)
            {
                delete siguientes[i]->definicion;
                siguientes[i]->definicion = nullptr;
                delete siguientes[i];
                siguientes[i] = nullptr;
            }
            else
                siguientes[i]->podarNodos();
        }
    }
}

template <typename T>
void string_map<T>::Nodo::borrarNodos()
{
    for (int i = 0; i < 256; i++)
    {
        if (siguientes[i] != nullptr)
        {
            if (siguientes[i]->cantHijos() == 0)
            {
                delete siguientes[i]->definicion;
                siguientes[i]->definicion = nullptr;
                delete siguientes[i];
                siguientes[i] = nullptr;
            }
            else
                siguientes[i]->podarNodos();
        }
    }
}

template<typename T>
void string_map<T>::eliminar()
{
    while (_size > 0)
    {
        string aux = masALaIzq();
        erase(aux);
    }
}

template <typename T>
int string_map<T>::Nodo::largoRamaIzq()
{
    int i = 0;
    bool encontro = false;
    while (i < 256 && !encontro)
    {
        if (siguientes[i] != nullptr)
        {
            encontro = true;
        }
        i++;
    }
    if (encontro)
        return 1 + siguientes[i - 1]->largoRamaIzq();
    else
        return 0;
}

template <typename T>
string string_map<T>::masALaIzq()
{
    Nodo *actual = raiz;
    int valor = actual->largoRamaIzq();
    string res = "";
    for (int i = 0; i < valor; i++)
    {
        int j = 0;
        bool encontro = false;
        while (!encontro)
        {
            if ((actual->siguientes)[j] != nullptr)
            {
                encontro = true;
            }
            j++;
        }
        j--;
        res.push_back((char)j);
        actual = (actual->siguientes)[j];
    }
    return res;
}

template <typename T>
void string_map<T>::insert(const pair<string, T> &elem)
{
    if (this->count(elem.first) == 0)
        _size++;
    if (this->raiz == nullptr)
    {
        this->raiz = new Nodo();
    }
    Nodo *actual = this->raiz;
    for (int i = 0; i < (elem.first).size(); i++)
    {
        int x = (int)((elem.first)[i]);
        if ((actual->siguientes)[x] == nullptr)
        {
            if (i == (elem.first).size() - 1)
            {
                T *copia = new T(elem.second);
                (actual->siguientes)[x] = new Nodo(copia);
            }
            else
            {
                Nodo *nuevo = new Nodo();
                (actual->siguientes)[x] = nuevo;
                actual = nuevo;
            }
        }
        else
        {
            if (i == (elem.first).size() - 1)
            {
                if (count(elem.first) == 1)
                {
                    delete ((actual->siguientes)[x])->definicion;
                    actual->definicion = nullptr;
                }
                T *copia = new T(elem.second);
                ((actual->siguientes)[x])->definicion = (copia);
            }
            else
                actual = (actual->siguientes)[x];
        }
    }
}

template <typename T>
T &string_map<T>::operator[](const string &clave)
{
    // COMPLETAR
}

template <typename T>
int string_map<T>::count(const string &clave) const
{
    // COMPLETAR
    if (raiz == nullptr)
    {
        return 0;
    }
    else
    {
        int i = 0;
        int res = 1;
        Nodo *actual = raiz;
        while (i < clave.size() && res == 1)
        {
            int x = (int)((clave)[i]);
            if (i != clave.size() - 1)
            {
                if ((actual->siguientes)[x] == nullptr)
                    res = 0;
                else
                {
                    actual = (actual->siguientes)[x];
                }
                i++;
            }
            else
            {
                if ((actual->siguientes)[x] == nullptr)
                    res = 0;
                else
                {
                    if ((actual->siguientes[x])->definicion == nullptr)
                        res = 0;
                }
                i++;
            }
        }
        return res;
    }
}

template <typename T>
const T &string_map<T>::at(const string &clave) const
{
    // COMPLETAR
    Nodo *actual = raiz;
    for (int i = 0; i < clave.size(); i++)
    {
        int x = (int)((clave)[i]);
        actual = (actual->siguientes)[x];
    }
    return (actual->definicion);
}

template <typename T>
T &string_map<T>::at(const string &clave)
{
    // COMPLETAR
    Nodo *actual = raiz;
    for (int i = 0; i < clave.size(); i++)
    {
        int x = (int)((clave)[i]);
        actual = (actual->siguientes)[x];
    }
    return *(actual->definicion);
}

template <typename T>
void string_map<T>::erase(const string &clave)
{
    // COMPLETAR
    for (int i = 0; i < clave.size(); i++)
    {
        Nodo *actual = raiz;
        int x = (int)(clave[clave.size() - i - 1]);
        for (int j = 0; j < clave.size() - i - 1; j++)
        {
            actual = (actual->siguientes)[(int)(clave[j])];
        }
        if (i == 0)
        {
            delete (actual->siguientes)[x]->definicion;
            (actual->siguientes)[x]->definicion = nullptr;
        }
        if ((actual->siguientes)[x]->cantHijos() == 0 && (actual->siguientes)[x]->definicion == nullptr)
        {
            delete (actual->siguientes)[x];
            (actual->siguientes)[x] = nullptr;
        }
    }
    if (_size == 1)
    {
        delete raiz;
        raiz = nullptr;
    }
    _size--;
}

template <typename T>
int string_map<T>::Nodo::cantHijos()
{
    int res = 0;
    for (int i = 0; i < 256; i++)
    {
        if (siguientes[i] != nullptr)
        {
            res++;
        }
    }
    return res;
}

template <typename T>
int string_map<T>::size() const
{
    // COMPLETAR
    return _size;
}

template <typename T>
bool string_map<T>::empty() const
{
    // COMPLETAR
    return _size == 0;
}