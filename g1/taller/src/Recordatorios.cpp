#include <iostream>
#include <list>
#include <vector>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes)
{
  uint dias[] = {
      // ene, feb, mar, abr, may, jun
      31, 28, 31, 30, 31, 30,
      // jul, ago, sep, oct, nov, dic
      31, 31, 30, 31, 30, 31};
  return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha
{
public:
  // Completar declaraciones funciones
  Fecha(int mes, int dia);
  int mes();
  int dia();


  bool operator==(Fecha o);


  void incrementar_dia();
  bool operator<(Fecha o);

private:
  //Completar miembros internos
  int mes_;
  int dia_;
};

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {}

int Fecha::mes()
{
  return mes_;
}

int Fecha::dia()
{
  return dia_;
}

ostream &operator<<(ostream &os, Fecha f)
{
  os << f.dia() << "/" << f.mes();
  return os;
}


bool Fecha::operator==(Fecha o)
{
  bool igual_dia = this->dia() == o.dia();
  // Completar iguadad (ej 9)
  bool igual_mes = this->mes() == o.mes();
  return igual_dia && igual_mes;
}


void Fecha::incrementar_dia()
{
  int days = dias_en_mes(mes_);
  if (days > dia_)
  {
    dia_++;
  }
  else
  {
    dia_ = 1;
    if (mes_ < 12)
    {
      mes_++;
    }
    else
    {
      mes_ = 1;
    }
  }
}

bool Fecha::operator<(Fecha o)
{
  if (this->mes() == o.mes())
  {
    return this->dia() < o.dia();
  }
  else
  {
    return this->mes() < o.mes();
  }
}
// Ejercicio 11, 12
class Horario
{
public:
  // Completar declaraciones funciones
  Horario(uint hora, uint min);
  uint hora();
  uint min();
  bool operator==(Horario o);
  bool operator<(Horario o);

private:
  //Completar miembros internos
  int hora_;
  int min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min)
{
}

uint Horario::hora()
{
  return hora_;
}

uint Horario::min()
{
  return min_;
}

ostream &operator<<(ostream &os, Horario h)
{
  os << h.hora() << ":" << h.min();
  return os;
}

bool Horario::operator==(Horario o)
{
  bool igual_hora = this->hora() == o.hora();
  // Completar iguadad (ej 9)
  bool igual_min = this->min() == o.min();
  return igual_hora && igual_min;
}

bool Horario::operator<(Horario o)
{
  if (this->hora() == o.hora())
  {
    return this->min() < o.min();
  }
  else
  {
    return this->hora() < o.hora();
  }
}
// Clase Horario

// Ejercicio 13

// Clase Recordatorio
class Recordatorio
{
public:
  // Completar declaraciones funciones
  Recordatorio(Fecha f, Horario h, string m);
  Fecha fecha();
  Horario horario();
  string mensaje();
  bool operator<(Recordatorio o);

private:
  //Completar miembros internos
  Fecha f_;
  Horario h_;
  string mensaje_;
};

Recordatorio::Recordatorio(Fecha f, Horario h, string m) : f_(f), h_(h), mensaje_(m){};

Fecha Recordatorio::fecha()
{
  return f_;
}

Horario Recordatorio::horario()
{
  return h_;
}

string Recordatorio::mensaje()
{
  return mensaje_;
}

ostream &operator<<(ostream &os, Recordatorio r)
{
  os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
  return os;
}

// Ejercicio 14

// Clase Agenda

class Agenda
{
public:
  Agenda(Fecha fecha_inicial);
  void agregar_recordatorio(Recordatorio rec);
  void incrementar_dia();
  list<Recordatorio> recordatorios_de_hoy();
  Fecha hoy();
  

private:
  Fecha fecha_inicial_;
  list<Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial) : fecha_inicial_(fecha_inicial)
{
}

void Agenda::agregar_recordatorio(Recordatorio rec)
{
  this->recordatorios_.push_back(rec);  
}

Fecha Agenda::hoy(){
  return fecha_inicial_;
}

void Agenda::incrementar_dia(){
  return fecha_inicial_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy()
{
  list<Recordatorio> res;
  for (Recordatorio r:recordatorios_)
  {
    if (r.fecha() == fecha_inicial_)
    {
      res.push_back(r);
    }    
  }
  res.sort();
  return res;
}

bool Recordatorio::operator<(Recordatorio o)
{
  if (this->fecha() == o.fecha())
  {
    return this->horario() < o.horario();
  }
  else
  {
    return this->fecha() < o.fecha();
  }
}

ostream &operator<<(ostream &os, list<Recordatorio> a)
{
  
  for(Recordatorio r: a)
  {
  os << r << endl;
  } 
  return os;
}

ostream &operator<<(ostream &os, Agenda a)
{
  os << a.hoy() << endl << "=====" << endl << a.recordatorios_de_hoy();
  return os;
}



