
template <class T>
Conjunto<T>::Conjunto()
{
    // Completar
    _raiz = nullptr;
}

template <class T>
Conjunto<T>::~Conjunto()
{
    while(_raiz != NULL)
        {
            this->remover((this->_raiz)->valor);
        }
}


template <class T>
bool Conjunto<T>::Nodo::perteneceEnNodo(const T &clave)
{
    if (this->valor == clave)
    {
        return true;
    }
    else if (this->valor < clave)
    {
        if (this->der == nullptr)
        {
            return false;
        }
        else
            (this->der)->perteneceEnNodo(clave);
    }
    else
    {
        if (this->izq == nullptr)
        {
            return false;
        }
        else
            (this->izq)->perteneceEnNodo(clave);
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T &clave) const
{
    if (_raiz == nullptr)
        return false;
    else
        return _raiz->perteneceEnNodo(clave);
}

template <class T>
void Conjunto<T>::Nodo::insertarEnNodo(const T &clave)
{
    if (clave < this->valor)
    {
        if (this->izq == nullptr)
        {
            Nodo *p = new Nodo(clave);
            this->izq = p;
            p->padre = this;
        }
        else
            (this->izq)->insertarEnNodo(clave);
    }
    else if (clave > this->valor)
    {
        if (this->der == nullptr)
        {
            Nodo *p = new Nodo(clave);
            this->der = p;
            p->padre = this;
        }
        else
            (this->der)->insertarEnNodo(clave);
    }
}

template <class T>
void Conjunto<T>::insertar(const T &clave)
{
    if (_raiz == nullptr)
    {
        _raiz = new Nodo(clave);
    }
    else if (!(this->pertenece(clave)))
        (this->_raiz)->insertarEnNodo(clave);
}

template <class T>
typename Conjunto<T>::Nodo *Conjunto<T>::Nodo::inmediatoInferior()
{
    return (this->izq)->masALaDerecha();
}

template <class T>
void Conjunto<T>::Nodo::removerNodo(const T &clave)
{
    if (this->valor == clave)
    {
        if (this->izq != nullptr)
        {
            Nodo *p = this->inmediatoInferior();
            if ((p->padre)->der != nullptr && (p->padre)->der == p)
            {
                (p->padre)->der = p->izq;
            }
            else
                (p->padre)->izq = p->izq;
            this->valor = p->valor;
            if ((p->izq) != nullptr)
                (p->izq)->padre = p->padre;
            delete p;
        }
        else if (this->der != nullptr)
        {
            Nodo *p = this->der;
            if ((p->der) != nullptr)
                (p->der)->padre = this;
            if ((p->izq) != nullptr)
                (p->izq)->padre = this;
            this->der = p->der;
            this->izq = p->izq;
            this->valor = p->valor;
            delete p;
        }
        else
        {
            Nodo *p = this;
            if ((p->padre)->der == p)
            {
                (p->padre)->der = nullptr;
                delete p;
            }
            else
            {
                (p->padre)->izq = nullptr;
                delete p;
            }
        }
    }
    else if (this->valor > clave)
        (this->izq)->removerNodo(clave);
    else
        (this->der)->removerNodo(clave);
}

template <class T>
void Conjunto<T>::remover(const T &clave)
{
    if (this->pertenece(clave))
    {
        if (_raiz->valor == clave)
        {
            if (this->cardinal() > 1)
            {
                if (_raiz->izq != nullptr)
                {
                    Nodo *p = _raiz->inmediatoInferior();
                    if ((p->padre)->izq != nullptr && (p->padre)->izq == p)
                    {
                        (p->padre)->izq = p->izq;
                    }
                    else
                        (p->padre)->der = p->izq;
                    if ((p->izq) != nullptr)
                        (p->izq)->padre = p->padre;
                    _raiz->valor = p->valor;
                    delete p;
                }
                else
                {
                    Nodo *p = _raiz->der;
                    _raiz->valor = p->valor;
                    _raiz->der = p->der;
                    _raiz->izq = p->izq;
                    if ((_raiz->der) != nullptr)
                        (_raiz->der)->padre = _raiz;
                    if ((_raiz->izq) != nullptr)
                        (_raiz->izq)->padre = _raiz;
                    delete p;
                }
            }
            else
            {
                delete _raiz;
                _raiz = nullptr;
            }


        }
        else if (_raiz->izq != nullptr && (_raiz->izq)->perteneceEnNodo(clave))
        {
            (_raiz->izq)->removerNodo(clave);
        }
        else
            (_raiz->der)->removerNodo(clave);
    }
}

template <class T>
typename Conjunto<T>::Nodo *Conjunto<T>::Nodo::siguiente(const T &clave)
{
    if (this->valor != clave)
    {
        if (clave < this->valor)
            (this->izq)->siguiente(clave);
        else
            (this->der)->siguiente(clave);
    }
    else
    {
        if (this->der != nullptr)
        {
            return (this->der)->masALaIzquierda();
        }
        Nodo *p = this->padre;
        Nodo *q = this;
        while (p != nullptr && q == p->der)
        {
            q = p;
            p = this->padre;
        }
        return p;
    }
}

template <class T>
const T &Conjunto<T>::siguiente(const T &clave)
{
    return (_raiz->siguiente(clave))->valor;
}

template <class T>
typename Conjunto<T>::Nodo *Conjunto<T>::Nodo::masALaIzquierda()
{
    if (this->izq == nullptr)
    {
        return this;
    }
    else
        return (this->izq)->masALaIzquierda();
}

template <class T>
const T &Conjunto<T>::minimo() const
{
    (_raiz->masALaIzquierda())->valor;
}

template <class T>
typename Conjunto<T>::Nodo *Conjunto<T>::Nodo::masALaDerecha()
{
    if (this->der == nullptr)
    {
        return this;
    }
    else
        return (this->der)->masALaDerecha();
}

template <class T>
const T &Conjunto<T>::maximo() const
{
    (_raiz->masALaDerecha())->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const
{
    if (this->_raiz == nullptr)
    {
        return 0;
    }
    else
    {
        (this->_raiz)->contarNodos();
    }
}

template <class T>
unsigned int Conjunto<T>::Nodo::contarNodos() const
{
    if (this->izq == nullptr && this->der == nullptr)
    {
        return 1;
    }
    else
    {
        if (this->izq == nullptr)
            return 1 + (this->der)->contarNodos();
        else if (this->der == nullptr)
            return 1 + (this->izq)->contarNodos();
        else
            return 1 + (this->izq)->contarNodos() + (this->der)->contarNodos();
    }
}

template <class T>
void Conjunto<T>::mostrar(std::ostream &) const
{
    assert(false);
}

template <class T>
Conjunto<T>::Nodo::Nodo(const T &v)
{
    valor = v;
    izq = nullptr;
    der = nullptr;
    padre = nullptr;
}
